# encoding: utf-8;
require 'mysql'
require 'rest-client'

begin
  con = Mysql.new('localhost', 'root', 'Quique_58', 'HeaderLabs')
  rs = con.query('SELECT * FROM RSS')
  rs.each_hash do |h|
    print "#{h['id']}:#{h['nombre']}="
    begin
      response = RestClient.get h['url']
      print "true\n"
    rescue => e
      pst = con.prepare "UPDATE RSS SET estatus = false WHERE id=?"
      pst.execute h['id']
      #puts "No funciona #{h['id']} => #{h['nombre']}"
      print "false\n"
    end
  end
rescue Mysql::Error => e
  puts e.errno
  puts e.error
ensure
  con.close if con
end  